import React, { Component } from 'react'
import SearchUI from './Search'
import { Row, Col } from 'antd'

import styles from './Header.module.less';
export default class Header extends Component {
    render() {
        console.log(styles.maxWidth)
        return (
            <>
                <Row className="new">
                    <Col className={styles.maxWidth}>
                        <SearchUI />
                    </Col>
                </Row>
            </>
        )
    }
}
