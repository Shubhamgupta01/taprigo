import React from 'react';
import './App.less';

// Custom Components
import HeaderUI from './Components/Header/Header'

const App = () => (
  <>
    <div className="container">
      <HeaderUI />
    </div>
  </>
);

export default App;
